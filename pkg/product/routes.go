package product

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/azonnix/authentication/api-getway/pkg/auth"
	"gitlab.com/azonnix/authentication/api-getway/pkg/config"
	"gitlab.com/azonnix/authentication/api-getway/pkg/product/routes"
)

func RegisterRoutes(e *gin.Engine, c *config.Config, authSvc *auth.ServiceClient) {
	a := auth.InitAuthMiddlewareConfig(authSvc)
	svc := &ServiceClient{
		Client: InitServiceClient(c),
	}

	routes := e.Group("/product")
	routes.Use(a.Required)
	routes.POST("/", svc.CreateProduct)
	routes.GET("/:id", svc.FindOne)
}

func (svc *ServiceClient) CreateProduct(ctx *gin.Context) {
	routes.CreateProduct(ctx, svc.Client)
}

func (svc *ServiceClient) FindOne(ctx *gin.Context) {
	routes.FindOne(ctx, svc.Client)
}
