package auth

import (
	"fmt"

	"gitlab.com/azonnix/authentication/api-getway/pkg/auth/pb"
	"gitlab.com/azonnix/authentication/api-getway/pkg/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceClient struct {
	Client pb.AuthServiceClient
}

func InitServiceClient(c *config.Config) pb.AuthServiceClient {
	cc, err := grpc.Dial(c.AuthSvcUrl, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		fmt.Println("Could need connect:", err)
	}

	return pb.NewAuthServiceClient(cc)
}
