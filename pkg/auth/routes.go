package auth

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/azonnix/authentication/api-getway/pkg/auth/routes"
	"gitlab.com/azonnix/authentication/api-getway/pkg/config"
)

func RegisterRoutes(e *gin.Engine, c *config.Config) *ServiceClient {
	svc := &ServiceClient{
		Client: InitServiceClient(c),
	}

	routes := e.Group("/auth")
	routes.POST("/register", svc.Register)
	routes.POST("/login", svc.Login)

	return svc
}

func (svc *ServiceClient) Register(ctx *gin.Context) {
	routes.Register(ctx, svc.Client)
}

func (svc *ServiceClient) Login(ctx *gin.Context) {
	routes.Login(ctx, svc.Client)
}
