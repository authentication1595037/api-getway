package order

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/azonnix/authentication/api-getway/pkg/auth"
	"gitlab.com/azonnix/authentication/api-getway/pkg/config"
	"gitlab.com/azonnix/authentication/api-getway/pkg/order/routes"
)

func RegisterRoutes(e *gin.Engine, c *config.Config, authSvc *auth.ServiceClient) {
	a := auth.InitAuthMiddlewareConfig(authSvc)
	svc := &ServiceClient{
		Client: InitServiceClient(c),
	}

	routes := e.Group("/order")
	routes.Use(a.Required)
	routes.POST("/", svc.CreateOrder)
}

func (svc *ServiceClient) CreateOrder(ctx *gin.Context) {
	routes.CreateOrder(ctx, svc.Client)
}
