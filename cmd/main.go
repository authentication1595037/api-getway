package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/azonnix/authentication/api-getway/pkg/auth"
	"gitlab.com/azonnix/authentication/api-getway/pkg/config"
	"gitlab.com/azonnix/authentication/api-getway/pkg/order"
	"gitlab.com/azonnix/authentication/api-getway/pkg/product"
)

func main() {
	c, err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	r := gin.Default()

	authSvc := *auth.RegisterRoutes(r, &c)
	order.RegisterRoutes(r, &c, &authSvc)
	product.RegisterRoutes(r, &c, &authSvc)

	r.Run(c.Port)
}
